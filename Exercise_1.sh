# first exercise in shell scripting
#!/bin/bash
# This Script is to copy content from file A to file B and then delete file A content.
# Usage: ./clearlogs
cp /var/log/messages /var/log/messages.old
cp /dev/null /var/log/messages
echo log file copies and cleaned up
exit 0